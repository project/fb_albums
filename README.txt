Facebook Albums
===============

Allows public Facebook Page Albums to be displayed
on a Drupal website.

Installation:
Normal installation procedures.

Configuration:
First, get the Page ID from any public Facebook
page and add it at /admin/config/services/fbalbums.
Then, create a Facebook app with a Developer
account with "Page Public Content Access"
permissions. Need help? We can set this up
for you - call (302) 329-8855.
