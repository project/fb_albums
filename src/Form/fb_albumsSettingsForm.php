<?php

namespace Drupal\fb_albums\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class fb_albumsSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'fb_albums.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fb_albums_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['fb_albums_pageid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Public Page ID'),
      '#default_value' => $config->get('fb_albums_pageid'),
      '#required' => TRUE,
    ];  

    $form['fb_albums_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Public Client ID'),
      '#default_value' => $config->get('fb_albums_client_id'),
      '#required' => TRUE,
    ];  

    $form['fb_albums_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Public Client Secret'),
      '#default_value' => $config->get('fb_albums_client_secret'),
      '#required' => TRUE,
    ];  




    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('fb_albums_pageid', $form_state->getValue('fb_albums_pageid'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->set('fb_albums_client_id', $form_state->getValue('fb_albums_client_id'))
      ->set('fb_albums_client_secret', $form_state->getValue('fb_albums_client_secret'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}