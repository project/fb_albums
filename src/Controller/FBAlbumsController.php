<?php
/**
 * @file
 * Contains \Drupal\fb_albums\Controller\FBAlbumsController.
 */

namespace Drupal\fb_albums\Controller;
class FBAlbumsController {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => $this->fbcontrol(),
    );
  }

  //get json
  public function getJson($url){
    $realm = file_get_contents($url);
    $realm = json_decode($realm);
    return $realm;
  }

  //get the token
  public function getToken(){
    $client_id = \Drupal::config('fb_albums.settings')->get('fb_albums_client_id');
    $client_secret = \Drupal::config('fb_albums.settings')->get('fb_albums_client_secret');
    $token = $this->getJson('https://graph.facebook.com/oauth/access_token?client_id='.$client_id.'&client_secret='.$client_secret.'&grant_type=client_credentials');
    return $token->access_token;
  }

  public function fbcontrol() {
    $pid = \Drupal::config('fb_albums.settings')->get('fb_albums_pageid');
    $action = $_GET['action']; //albums; photos
    $aid = $_GET['aid'];

    $token = $this->getToken();

    $content="";
    $ccount=1;
    $col=3;
    switch($action){
      default:
      //   $content .= 'You must configure this module. [<a href="#">Click here</a>]';
      // break;
      // case 'albums':
        $albums = $this->getJson('https://graph.facebook.com/v5.0/'.$pid.'/albums?&access_token='.$token);
        $albums = $albums->data;
        foreach($albums as $album){
          $content .= '<div><a href="/fbalbums?action=photos&albumName='.$album->name.'&aid='.$album->id.'">'.$album->name.'</a></div>';
        }
      break;
      case 'photos':
        $content .= '<h2 class="album-title">'.$_GET['albumName'].'</h2>';
        $content .= '<div><em><a href="/fbalbums">Back to Album List</a></em></div>';
        if($_GET['before']){$paginationCode='&before='.$_GET['before'];}
        if($_GET['after']){$paginationCode='&after='.$_GET['after'];}
        $photos = $this->getJson('https://graph.facebook.com/v5.0/'.$aid.'/photos?fields=source,name&limit=25&access_token='.$token.$paginationCode);
        $before = $photos->paging->cursors->before;
        $after = $photos->paging->cursors->after;
        if($photos->paging->previous){$pagination[] = '<a class="btn btn-primary" href="/fbalbums?action=photos&albumName='.$_GET['albumName'].'&aid='.$aid.'&before='.$before.'">Previous</a>';}
        if($photos->paging->next){$pagination[] = '<a class="btn btn-primary" href="/fbalbums?action=photos&albumName='.$_GET['albumName'].'&aid='.$aid.'&after='.$after.'">Next</a>';}
        $photos = $photos->data;
        $content .= '<div class="row">';
        $pagination = implode(' | ',$pagination);
        $content .= '<div class="row"><div class="fb-pagination">'.$pagination.'</div></div>';
        foreach($photos as $photo){
          $content .= '<div class="col-sm-'.$col.'"><div class="fb-albums-photoblock">';
          $content .= '<div><a href="#" class="fbimgitem" data-fbimgurl="'.$photo->source.'"><img class="img-responsive" src="'.$photo->source.'"></a></div>';
          $content .= '<div><a href="#" class="fbimgitem" data-fbimgurl="'.$photo->source.'">'.$photo->name.'</a></div>';
          $content .= '</div></div>';
          if($ccount==$col+1){
            $content .= '</div><div class="row">';
            $ccount=0;
          }
          $ccount++;
        }
        $content .= '</div>';
        $content .= '<div class="row"><div class="fb-pagination">'.$pagination.'</div></div>';
      break;
      case 'photo':
      break;
    }
    $content = '<div id="fbalbums">'.$content.'</div>';
    return $content;
  }
}
