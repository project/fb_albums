jQuery(document).ready(function($) {
  $(".fbimgitem").click(function(e) {
    e.preventDefault();
    var url = $(this).data("fbimgurl");
    $("body").prepend(
      '<div class="fbImg"><img class="img-responsive" src="' +
        url +
        '"><button onClick="closeButton();" id="closeAsset" type="button" class="btn btn-primary">Close</button></div>'
    );
  });
});

function closeButton() {
  $(".fbImg").remove();
}
